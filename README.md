# k4b4 mk2 bootloader #

This is a part of k4b4 mk2 tiny USB-MIDI controller.

* USB-HID bootloader 
* Based on USB-HID bootloader in [Microchip Libraries for Application](http://www.microchip.com/pagehandler/en-us/devtools/mla/home.html)

## License ##
See [Microchip Libraries for Application](http://www.microchip.com/pagehandler/en-us/devtools/mla/home.html)